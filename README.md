# Vodacom Contacts API

* 1. Install python 3
* 2. Install python-pip
* 3. Clone Project locally.
* 4. Change directory to project directory ( on the terminal)
* 5. run: `pip install -r requirements.txt`
* 6. Database is a sqlite3 database. I have also committed it as to eliminate the need to re-run migrations. Should you have an issue with the database (db.sqlite3). 
 and Delete it and run: `python manage.py migrate`.
* 7. run: `python manage.py runserver 0.0.0.0:5000` or deploy on docker: `docker-compose up` and it will bind to the same port: `5000`
* 8. Once project is running: open url: http://localhost:5000/admin on browser. Default username/password: root/root.
* Please let me know if you have any questions or any issues. (brianmafu@gmail.com/ +2765 911 0631)
