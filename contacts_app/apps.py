from django.apps import AppConfig


class ContactsAppAppConfig(AppConfig):
    name = 'contacts_app'
