from rest_framework import serializers
from contacts_app.models import Contact

class  ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = [
            'id',
            'title',
            'name',
            'email',
            'phone',
            'created',
            'active',
            'description'
        ]
  
    def create(self, data):
        title = data['title']
        name = data['name']
        email = data['email']
        phone = data['phone']
        description = data['description']

        contact = Contact(
            title=title,
            name=name,
            email=email,
            phone=phone,
            active=True,
            description=description
        )
        contact.save()
        data['id'] =  contact.id
        return data

    def update(self, instance, data):
        instance.title = data.get('title', instance.title)
        instance.name = data.get('name', instance.name)
        instance.email = data.get('email', instance.email)
        instance.phone = data.get('phone', instance.phone)
        instance.active = data.get('active', instance.active)
        instance.description = data.get('description', instance.description)


        instance.save()

        return instance