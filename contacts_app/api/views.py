from rest_framework.generics import (
    ListAPIView, CreateAPIView, RetrieveUpdateAPIView,
    RetrieveAPIView, DestroyAPIView
)
from django.db.models import Q
from rest_framework import pagination
from rest_framework.permissions import (IsAuthenticatedOrReadOnly, IsAuthenticated)
from .serializers import ContactSerializer
from contacts_app.models import Contact
from rest_framework.pagination import LimitOffsetPagination


class ContactListAPIView(ListAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = ContactSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self, *args, **kwargs):
        queryset_list = Contact.objects.all()
        page_size = 'page_size'
        if self.request.GET.get(page_size):
            pagination.PageNumberPagination.page_size = self.request.GET.get(page_size)
        else:
            pagination.PageNumberPagination.page_size = 10
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(
                Q(email__icontains=query) |
                Q(title_icontains=query) |
                Q(name__icontains=query) |
                Q(phone__icontains=query)


            )

        return queryset_list.order_by('-id')


class ContactCreateAPIView(CreateAPIView):
    serializer_class = ContactSerializer
    #permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Contact.objects.all()


class ContactDetailAPIView(RetrieveAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


class ContactDeleteAPIView(DestroyAPIView):
    queryset = Contact.objects.all()
    #permission_classes = [IsAuthenticated]
    serializer_class = ContactSerializer


class UpdateAPIView(RetrieveUpdateAPIView):
    #permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)