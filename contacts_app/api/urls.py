from django.urls import path

from .views import (
    ContactListAPIView, ContactCreateAPIView,
    ContactDetailAPIView, ContactDeleteAPIView,
    UpdateAPIView
)


urlpatterns = [
    path('', ContactListAPIView.as_view(), name='contact-list'),
    path('create', ContactCreateAPIView.as_view(), name='contact-create'),
    path('profile/<int:pk>/', ContactDetailAPIView.as_view(), name='contact-details'),
    path('delete/<int:pk>/', ContactDeleteAPIView.as_view(), name='contact-delete'),
    path('update/<int:pk>/', UpdateAPIView.as_view(), name='contact-update')
]