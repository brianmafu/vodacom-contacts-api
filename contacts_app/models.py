from django.db import models
from datetime import datetime
# Create your models here.


class Contact(models.Model):
    title = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    phone = models.CharField(max_length=250)
    created = models.DateField(default=datetime.now().strftime("%Y-%m-%d"))
    active =  models.BooleanField(default=False)
    description = models.CharField(max_length=300, blank=True, null=True)
    class Meta:
        ordering = ["-created"]
    def __str__(self):
        return self.name